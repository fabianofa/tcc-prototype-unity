﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.IO.Ports;

public class prototype : MonoBehaviour {

    //SerialPort stream = new SerialPort("\\\\.\\COM4", 9600, Parity.None, 8, StopBits.One);
    SerialPort stream = new SerialPort("COM4", 9600, Parity.None, 8, StopBits.One);

    // Use this for initialization
    void Start () {

        stream.ReadTimeout = 1000;
        stream.Open();
	}
	
	// Update is called once per frame
	void Update () {
        // Debug.Log(stream.CheckOpen("COM4", 9600));
        string value = stream.ReadLine();
        string[] values = value.Split(';');


        Quaternion target = Quaternion.Euler(float.Parse(values[1]) * -1, float.Parse(values[2]), float.Parse(values[0]));
        transform.rotation = Quaternion.Slerp(transform.rotation, target, Time.deltaTime);
        Debug.Log(value);
	}
}
